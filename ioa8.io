
//opens file, sends to hamming dist func,writes new file output
func1 := method(
	f := File with("tst.txt")
	f openForReading

	l := f readLines
	l println

	i := l size

	c := 0
	combinedlist := list()
	while(i > 0,
		mylist := (l at(c) split)
		dist := func2(mylist)
		mylist append(dist)
		combinedlist append(mylist)
		c = c + 1
		i = i - 1
	)
	f close
	e := File with ("tst_output.txt")
	e openForUpdating

	listasString := combinedlist join
	e write(listasString)
	e close
)

//calculates the hamming distance for a list
func2 := method(mylist,
	number := mylist at(0) asNumber
	number2 := mylist at(1) asNumber
	dist := 0
	val := (number ^ number2)
		while(val != 0,
		val = val &(val - 1)
		dist = dist + 1
	)
	return dist
)
func1()