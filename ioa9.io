csvReader := method(
	f := File with("test.csv")
	f openForReading

	csVRow := f readLines
	csVRow println

	i := csVRow size

	c := 0
	while(i > 0,
		csVRow at(c) println
		c = c + 1
		i = i - 1
	)
	f close
	//mylist println
	)
csvReader()