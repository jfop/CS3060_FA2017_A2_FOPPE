Shape := Object clone
Shape color := "red"
Shape filled := true

Shape := method(
)
Shape := method(color,filled,
	Shape color := color
	Shape filled := filled
)
Shape getColor := method(
	return Shape color
)
Shape setColor := method(color,
	Shape color := color
)
Shape isFilled := method(
	return Shape filled
)
Shape setFilled := method(filled,
	Shape filled := filled
)

Circle := Shape clone
Circle radius := 1.0

Circle := method(
)
Circle := method(radius,
	Circle radius := radius
)
Circle := method(radius,color,filled,
	Circle radius := radius
	Circle color := color
	Circle filled := filled
)
Circle getRadius := method(
	return Circle radius
)
Circle setRadius := method(radius,
	Circle radius := radius
)
Circle getArea := method(
	return (3.14 * (Circle radius / 2))
)
Circle getPerimeter := method(
	return (2 * 3.14 * Circle radius)
)

Rectangle := Shape clone
Rectangle width := 1.0
Rectangle length := 1.0

Rectangle := method(
)
Rectangle := method(width, length,
	Rectangle width := width
	Rectangle length := length
)
Rectangle := method(width,length,color,filled,
	Rectangle width := width
	Rectangle length := length
	Rectangle color := color
	Rectangle filled := filled
)
Rectangle getWidth := method(
	return Rectangle width
}
Rectangle setWidth := method(width,
	Rectangle width := width
)
Rectangle getLength := method(
	return Rectangle length
)
Rectangle setLength := method(length,
	Rectangle length := length
)
Rectangle getArea := method(
	return (Rectangle length * Rectangle width)
)
Rectangle getPerimeter := method(
	return (Rectangle width + Rectangle width + Rectangle length + Rectangle length)
)

Square := Rectangle clone

Square := method(
)
Square := method (side,
	Square side := side
)
Square := method (side,color,filled,
	Square side := side
	Square color := color
	Square filled := filled
)
Square getSide := method(
	return Square side
)
Square setSide := method(side,
	Square side := side
)