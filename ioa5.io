func1 := method(i,
  if(i < 3, 1, 
  func1(i - 1) + func1(i - 2))
)
for(i, 0, 500, func1(i) println)